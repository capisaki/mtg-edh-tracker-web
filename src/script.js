class App {
    constructor() {
        this.templates = document.createElement( 'template' );

        fetch('templates.html').then(response => response.text()).then((text) => {
            this.templates.innerHTML = text;

            app.playerSelection();
        });
    }

    getTemplate = function(templateName) {
        let template = app.templates.content.getElementById(templateName);
        if (template) {
            let finalTemplate = template.content.cloneNode(true);
            return finalTemplate;
        }

        return undefined;
    }

    // Go to player selection
    playerSelection = function (){
        let playerSelection = this.getTemplate('player-selection-template');
        playerSelection.querySelectorAll('.selection').forEach(element => {
            if (['three', 'four'].includes(element.dataset['number'])) {
                element.addEventListener('click', event => {
                    let number = app.stringIntoNumber(event.target.dataset['number']);
                    let players = [];
                    for (let index = 1; index <= number; index++) {
                        players.push(app.playerLife(index, event.target.dataset['number']));
                    }

                    app.gameboard(players);
                });
            }
        });
        app.applyContent(playerSelection);
    }

    gameboard = function(playerLifes) {
        let gameboard = this.getTemplate('gameboard-template');
        for (let index = 0; index < playerLifes.length; index++) {
            const element = playerLifes[index];
            gameboard.querySelector('.gameboard').append(element);
        }

        let settings = app.settings(playerLifes.length);
        gameboard.querySelector('.gameboard').append(settings);

        app.applyContent(gameboard);
    }

    settings = function(playerNumber) {
        let settings = this.getTemplate('settings-template');
        settings.querySelector('#settings-icon').classList.add(app.numberIntoString(playerNumber) + '-players');
        settings.querySelector('#settings-icon').addEventListener('click', event => {
            event.target.closest('.gameboard').querySelector('#settings').style.visibility = 'visible';
        });
        settings.querySelector('#settings-background').addEventListener('click', event => {
            event.target.closest('.gameboard').querySelector('#settings').style.visibility = 'hidden';
        });
        settings.querySelector('#settings-close').addEventListener('click', event => {
            event.target.closest('.gameboard').querySelector('#settings').style.visibility = 'hidden';
        });
        settings.querySelector('#settings-restart').addEventListener('click', event => {
            app.playerSelection();
        });
        settings.querySelector('#settings-fullscreen').addEventListener('click', event => {
            document.fullscreenElement === null ? app.openFullscreen() : app.closeFullscreen();
        });

        return settings;
    }

    // DEBUG FUNCTION
    playerLife = function (playerNumber, totalPlayers){
        let playerLife = this.getTemplate('player-life-template');
        
        playerLife.firstElementChild.classList.add(totalPlayers + '-players');
        playerLife.firstElementChild.classList.add('player-life-p' + playerNumber);
        playerLife.firstElementChild.setAttribute('id', 'player-life-p' + playerNumber);

        for (let index = 1; index <= app.stringIntoNumber(totalPlayers); index++) {
            if (index !== playerNumber) {
                let enemyCommanderDmgTemplate = this.getTemplate('enemy-commander-dmg-template');
                enemyCommanderDmgTemplate.querySelector('.enemy-commander-dmg').classList.add('player-life-p' + index);
                enemyCommanderDmgTemplate.querySelector('.enemy-commander-dmg-number').classList.add('enemy-commander-dmg-number-p' + index);
                enemyCommanderDmgTemplate.querySelector('.enemy-commander-up').dataset['targetClass'] = 'enemy-commander-dmg-number-p' + index;
                enemyCommanderDmgTemplate.querySelector('.enemy-commander-down').dataset['targetClass'] = 'enemy-commander-dmg-number-p' + index;
                playerLife.querySelector('.enemies-commander-dmg').appendChild(enemyCommanderDmgTemplate);
            }
        }

        // Apply up/down life
        playerLife.querySelector('.player-life-up').addEventListener('click', app.addLife);
        playerLife.querySelector('.player-life-down').addEventListener('click', app.substractLife);

        // Apply up/down to all commander damage
        playerLife.querySelectorAll('.enemy-commander-up').forEach(element => {
            element.addEventListener('click', app.addLife);
        });
        playerLife.querySelectorAll('.enemy-commander-down').forEach(element => {
            element.addEventListener('click', app.substractLife);
        });

        // Apply up/down poison
        playerLife.querySelector('.poison-counter-up').addEventListener('click', app.addLife);
        playerLife.querySelector('.poison-counter-down').addEventListener('click', app.substractLife);

        let gesture = new TinyGesture(playerLife.querySelector('.player-wrap'), {
            mouseSupport: true,
        });

        let swipeShowEvent = app.getSwipeShowEvent(playerNumber, totalPlayers);
        let swipeHideEvent = app.getSwipeHideEvent(playerNumber, totalPlayers);

        gesture.on(swipeShowEvent, event => {
            event.target.closest('.player-wrap').querySelector('.player-second-screen').style.left = 0;
        });

        gesture.on(swipeHideEvent, event => {
            event.target.closest('.player-wrap').querySelector('.player-second-screen').style.left = app.getLeftQuantity();
        });

        return playerLife;
    }

    getLeftQuantity = function() {
        return document.querySelector('.player-life').classList.contains('three-players') ? '60vw' : '50vw';
    }

    getSwipeShowEvent = function(playerNumber, totalPlayers) {
        switch (totalPlayers) {
            case 'three':
                switch (playerNumber) {
                    case 1:
                        return 'swiperight'
                    case 2:
                        return 'swipeleft';
                    case 3:
                        return 'swipedown';
                }
            case 'four':
                return [2,4].includes(playerNumber) ? 'swipeleft' : 'swiperight';
        }
        return '';
    }

    getSwipeHideEvent = function(playerNumber, totalPlayers) {
        switch (totalPlayers) {
            case 'three':
                switch (playerNumber) {
                    case 1:
                        return 'swipeleft'
                    case 2:
                        return 'swiperight';
                    case 3:
                        return 'swipeup';
                }
            case 'four':
                return [2,4].includes(playerNumber) ? 'swiperight' : 'swipeleft';
        }
        return '';
    }

    addLife = function(event) {
        // hack: only act on single clicks
        if (event.detail === 2) {
            return;
        }
        let life = event.target.closest('.player-main-screen').querySelector("."+event.target.dataset['targetClass']).innerText;
        life = parseInt(life);
        life++
        event.target.closest('.player-main-screen').querySelector("."+event.target.dataset['targetClass']).innerText = life;

        if (event.target.dataset['targetClass'].match(/^enemy-commander/)) {
            let wholeLife = event.target.closest('.player-main-screen').querySelector('.player-life-number').innerText;
            wholeLife = parseInt(wholeLife);
            wholeLife--;
            event.target.closest('.player-main-screen').querySelector('.player-life-number').innerText = wholeLife;
        }

    }

    substractLife = function(event) {
        // hack: only act on single clicks
        if (event.detail === 2) {
            return;
        }
        let life = event.target.closest('.player-main-screen').querySelector("."+event.target.dataset['targetClass']).innerText;
        life = parseInt(life);
        life--
        event.target.closest('.player-main-screen').querySelector("."+event.target.dataset['targetClass']).innerText = life;

        if (event.target.dataset['targetClass'].match(/^enemy-commander/)) {
            let wholeLife = event.target.closest('.player-main-screen').querySelector('.player-life-number').innerText;
            wholeLife = parseInt(wholeLife);
            wholeLife++;
            event.target.closest('.player-main-screen').querySelector('.player-life-number').innerText = wholeLife;
        }
    }

    applyContent = function(content) {
        document.getElementById('root').innerHTML = '';
        document.getElementById('root').append(content);
    }

    stringIntoNumber = function(number) {
        switch (number) {
            case 'two':
                return 2;
            case 'three':
                return 3;
            case 'four':
                return 4;
            case 'five':
                return 5;
            case 'six':
                return 6;
            default:
                return -1;
        }
    }

    numberIntoString = function(number) {
        switch (number) {
            case 2:
                return 'two';
            case 3:
                return 'three';
            case 4:
                return 'four';
            case 5:
                return 'five';
            case 6:
                return 'six';
            default:
                return -1;
        }
    }

    openFullscreen = function() {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.webkitRequestFullscreen) { /* Safari */
            document.documentElement.webkitRequestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) { /* IE11 */
            document.documentElement.msRequestFullscreen();
        }
    }
    
    /* Close fullscreen */
    closeFullscreen = function() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) { /* Safari */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE11 */
            document.msExitFullscreen();
        }
    }
}

const app = new App();

function deviceOrientation() {
    var body = document.querySelector('#root');
    body.classList = '';
    console.log(screen.orientation);
    switch(window.orientation) {
      case 0:
        body.classList.add('rotation90');
        break;
      case 180:
        body.classList.add('rotation-90');
        break;
      default:
        body.classList.add('portrait');
        break;
    }
  }
  window.addEventListener('orientationchange', deviceOrientation);
  deviceOrientation();