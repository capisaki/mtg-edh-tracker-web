# # If the first argument is "run"...
# ifeq (npm,$(firstword $(MAKECMDGOALS)))
#   # use the rest as arguments for "run"
#   NPM_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
#   # ...and turn them into do-nothing targets
#   $(eval $(NPM_ARGS):;@:)
# endif

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

startdev: ## Execute docker
	docker-compose up -d

stopdev: ## Execute docker
	docker-compose down