# MTG EDH Tracker Web

## ToDo
- Player's life
  - Upper half: add life
  - Lower half: substract life
  - Press more than 2s: +-5 (5, 10, 15)
- Enemies' Commander damage dealt
  - Automatic sustraction of player's life
  - Counter works as above (halfs and press)
- Poison counters
- Dice roll
- Optional player's database
  - Nick of player
  - Deck to use
  - Background for player/deck
- Game log (v2)
- Timer (v2)
- Double commander (v2)
- Symmetric Commander damage points (to be thinked)

## Where to pick ideas
### Style: MTG Life Counter App: Lotus (iOS)
- Simple style
- Plain colors
- Some gaps between players
- Shadow on touch (fix, I think)
### Functionality: Commander Tracker (Android)
- Commander damage affects your life
### Functionality: Moxfield (iOS)
- Counters for everything (poison, xp, storm,...) on commander swipe

## Mock
![Player screen mock](./assets/mock_player.png)

## Acknowledgements
[Include Media](https://github.com/eduardoboucas/include-media) by [Eduardo Bouças](https://github.com/eduardoboucas), and [more](https://github.com/eduardoboucas/include-media#the-authors)